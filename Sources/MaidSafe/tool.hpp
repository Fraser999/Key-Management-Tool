/*  Copyright 2015 MaidSafe.net limited.

    This SAFE Network Software is licensed to you under (1) the MaidSafe.net Commercial License,
    version 1.0 or later, or (2) The General Public License (GPL), version 3, depending on which
    licence you accepted on initial access to the Software (the "Licences").

    By contributing code to the SAFE Network Software, or to this project generally, you agree to be
    bound by the terms of the MaidSafe Contributor Agreement, version 1.0.  This, along with the
    Licenses can be found in the root directory of this project at LICENSE, COPYING and CONTRIBUTOR.

    Unless required by applicable law or agreed to in writing, the SAFE Network Software distributed
    under the GPL Licence is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
    ANY KIND, either express or implied.

    Please review the Licences for the specific language governing permissions and limitations
    relating to use of the SAFE Network Software.                                                 */

#ifndef TOOL_HPP_
#define TOOL_HPP_

#include <memory>
#include <string>
#include <vector>

#include "Commands/command.hpp"

struct Tool {
  Tool();
  std::unique_ptr<Command> current_command;
  bool reconstruct;
  int total_share_count, threshold;
  std::vector<std::string> secret_shares;
  std::string path_to_company_key, company_password;
  const static int kQuit_ = 0;
};

#endif  // TOOL_HPP_
