/*  Copyright 2015 MaidSafe.net limited.

    This SAFE Network Software is licensed to you under (1) the MaidSafe.net Commercial License,
    version 1.0 or later, or (2) The General Public License (GPL), version 3, depending on which
    licence you accepted on initial access to the Software (the "Licences").

    By contributing code to the SAFE Network Software, or to this project generally, you agree to be
    bound by the terms of the MaidSafe Contributor Agreement, version 1.0.  This, along with the
    Licenses can be found in the root directory of this project at LICENSE, COPYING and CONTRIBUTOR.

    Unless required by applicable law or agreed to in writing, the SAFE Network Software distributed
    under the GPL Licence is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
    ANY KIND, either express or implied.

    Please review the Licences for the specific language governing permissions and limitations
    relating to use of the SAFE Network Software.                                                 */

#ifndef COMMAND_HPP_
#define COMMAND_HPP_

#include <iostream>
#include <memory>
#include <string>

struct Tool;

class Command {
 public:
  Command(Tool* tool, std::string preamble, std::string instructions);
  virtual ~Command() {}
  virtual void GetChoice() = 0;
  virtual std::unique_ptr<Command> HandleChoice() = 0;

 protected:
  template <typename Choice, typename... Args>
  bool DoGetChoice(Choice& choice, const Choice* const default_choice, Args... args);

  template <typename Choice, typename... Args>
  void ConvertAndValidateChoice(const std::string& choice_as_string, Choice& choice,
                                const Choice* const default_choice, Args... args);

  Tool* tool_;
  const std::string kPreamble_, kInstructions_;
  static const std::string kPrompt_, kQuitCommand_, kSeparator_;

 private:
  std::string GetLine();
  void CheckForExitCommand(std::string input_command) const;
};



template <typename Choice, typename... Args>
bool Command::DoGetChoice(Choice& choice, const Choice* const default_choice, Args... args) {
  std::string line(GetLine());
  try {
    ConvertAndValidateChoice(line, choice, default_choice, args...);
    return true;
  } catch (...) {
    std::cout << "\n" << line << " is not a valid choice.\n";
    return false;
  }
}

template <>
void Command::ConvertAndValidateChoice<int, int, int>(const std::string& choice_as_string,
                                                      int& choice, const int* const default_choice,
                                                      int min, int max);

template <>
void Command::ConvertAndValidateChoice<bool>(const std::string& choice_as_string, bool& choice,
                                             const bool* const default_choice);

template <>
void Command::ConvertAndValidateChoice<std::string>(const std::string& choice_as_string,
                                                    std::string& choice,
                                                    const std::string* const default_choice);

#endif  // COMMAND_HPP_
