/*  Copyright 2015 MaidSafe.net limited.

    This SAFE Network Software is licensed to you under (1) the MaidSafe.net Commercial License,
    version 1.0 or later, or (2) The General Public License (GPL), version 3, depending on which
    licence you accepted on initial access to the Software (the "Licences").

    By contributing code to the SAFE Network Software, or to this project generally, you agree to be
    bound by the terms of the MaidSafe Contributor Agreement, version 1.0.  This, along with the
    Licenses can be found in the root directory of this project at LICENSE, COPYING and CONTRIBUTOR.

    Unless required by applicable law or agreed to in writing, the SAFE Network Software distributed
    under the GPL Licence is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
    ANY KIND, either express or implied.

    Please review the Licences for the specific language governing permissions and limitations
    relating to use of the SAFE Network Software.                                                 */

#include "command.hpp"

#include <algorithm>
#include <cassert>

#include "tool.hpp"

const std::string Command::kPrompt_ = "\n>> ";
const std::string Command::kQuitCommand_ = "q";
const std::string Command::kSeparator_ =
    "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";

Command::Command(Tool* tool, std::string preamble, std::string instructions)
    : tool_(tool),
      kPreamble_(std::move(preamble)),
      kInstructions_("\n\n" + kPreamble_ + std::move(instructions)) {
  assert(!std::any_of(kPreamble_.begin(), kPreamble_.end(), [](char c) { return c == '\n'; }));
}

std::string Command::GetLine() {
  std::string line;
  // On Unix, when a child process stops, it causes the eof bit to get set in std::cin.
  const int kMaxClearAttempts{1000};
  int clear_attempts{0};
  while (!std::getline(std::cin, line) && clear_attempts < kMaxClearAttempts) {
    if (std::cin.eof()) {
      std::cin.clear();
      ++clear_attempts;
    } else {
      std::cout << "Error reading from std::cin.\n";
      throw 1;
    }
  }
  CheckForExitCommand(line);
  return line;
}

template <>
void Command::ConvertAndValidateChoice<int, int, int>(const std::string& choice_as_string,
                                                      int& choice, const int* const default_choice,
                                                      int min, int max) {
  if (choice_as_string.empty() && default_choice)
    choice = *default_choice;
  else
    choice = std::stoi(choice_as_string);

  if (choice < min || choice > max)
    throw 2;
}

template <>
void Command::ConvertAndValidateChoice<bool>(const std::string& choice_as_string, bool& choice,
                                             const bool* const default_choice) {
  if (choice_as_string.empty() && default_choice) {
    choice = *default_choice;
  } else if (choice_as_string == "y" || choice_as_string == "Y") {
    choice = true;
  } else if (choice_as_string == "n" || choice_as_string == "N") {
    choice = false;
  } else {
    throw 3;
  }
}

template <>
void Command::ConvertAndValidateChoice<std::string>(const std::string& choice_as_string,
                                                    std::string& choice,
                                                    const std::string* const default_choice) {
  if (choice_as_string.empty() && !default_choice)
    throw 4;
  choice = choice_as_string.empty() ? *default_choice : choice_as_string;
}

void Command::CheckForExitCommand(std::string input_command) const {
  std::transform(input_command.begin(), input_command.end(), input_command.begin(), ::tolower);
  if (input_command == kQuitCommand_)
    throw Tool::kQuit_;
}
