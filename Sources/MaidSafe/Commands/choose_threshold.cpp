/*  Copyright 2015 MaidSafe.net limited.

    This SAFE Network Software is licensed to you under (1) the MaidSafe.net Commercial License,
    version 1.0 or later, or (2) The General Public License (GPL), version 3, depending on which
    licence you accepted on initial access to the Software (the "Licences").

    By contributing code to the SAFE Network Software, or to this project generally, you agree to be
    bound by the terms of the MaidSafe Contributor Agreement, version 1.0.  This, along with the
    Licenses can be found in the root directory of this project at LICENSE, COPYING and CONTRIBUTOR.

    Unless required by applicable law or agreed to in writing, the SAFE Network Software distributed
    under the GPL Licence is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
    ANY KIND, either express or implied.

    Please review the Licences for the specific language governing permissions and limitations
    relating to use of the SAFE Network Software.                                                 */

#include "choose_threshold.hpp"

#include <iostream>
#include <memory>

#include "enter_company_password.hpp"
#include "tool.hpp"

ChooseThreshold::ChooseThreshold(Tool* tool)
    : Command(tool, "Choose threshold.",
              "\nThis is the minimum number of secret shares required to reconstruct the company\n"
              "key and password.  It must be at least 2 and at most " +
                  std::to_string(tool->total_share_count) + ".\n" + kPrompt_) {}

void ChooseThreshold::GetChoice() {
  std::cout << kInstructions_;
  while (!DoGetChoice(tool_->threshold, static_cast<int*>(nullptr), 2, tool_->total_share_count))
    std::cout << '\n' << kInstructions_;
}

std::unique_ptr<Command> ChooseThreshold::HandleChoice() {
  std::cout << kSeparator_;
  return std::make_unique<EnterCompanyPassword>(tool_);
}
