/*  Copyright 2015 MaidSafe.net limited.

    This SAFE Network Software is licensed to you under (1) the MaidSafe.net Commercial License,
    version 1.0 or later, or (2) The General Public License (GPL), version 3, depending on which
    licence you accepted on initial access to the Software (the "Licences").

    By contributing code to the SAFE Network Software, or to this project generally, you agree to be
    bound by the terms of the MaidSafe Contributor Agreement, version 1.0.  This, along with the
    Licenses can be found in the root directory of this project at LICENSE, COPYING and CONTRIBUTOR.

    Unless required by applicable law or agreed to in writing, the SAFE Network Software distributed
    under the GPL Licence is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
    ANY KIND, either express or implied.

    Please review the Licences for the specific language governing permissions and limitations
    relating to use of the SAFE Network Software.                                                 */

#include "enter_company_password.hpp"

#include <iostream>
#include <memory>

#include "create_user_password.hpp"
#include "tool.hpp"
#include "utils.hpp"

EnterCompanyPassword::EnterCompanyPassword(Tool* tool)
    : Command(tool, std::string(tool->company_password.empty() ? "Enter" : "Confirm") +
                  " the company password.",
              (tool->company_password.empty() ?
                   "\nThis is the password for the MaidSafe company key.\n" :
                   "") +
                  kPrompt_),
      password_confirmed_(false) {}

void EnterCompanyPassword::GetChoice() {
  std::cout << kInstructions_;
  EchoConsoleInput(false);
  std::string password;
  while (!DoGetChoice(password, static_cast<std::string*>(nullptr)))
    std::cout << '\n' << kInstructions_;
  EchoConsoleInput(true);

  if (tool_->company_password.empty()) {
    tool_->company_password = password;
  } else if (tool_->company_password != password) {
    std::cout << "\n!!! Passwords don't match.  Start over!!!\n";
    tool_->company_password.clear();
  } else {
    password_confirmed_ = true;
  }
}

std::unique_ptr<Command> EnterCompanyPassword::HandleChoice() {
  if (password_confirmed_) {
    GenerateShares(tool_);
    std::cout << '\n' << kSeparator_;
    return std::make_unique<CreateUserPassword>(tool_, 0);
  }
  if (tool_->company_password.empty())  // User failed to enter same password twice
    std::cout << kSeparator_;
  return std::make_unique<EnterCompanyPassword>(tool_);
}
