/*  Copyright 2015 MaidSafe.net limited.

    This SAFE Network Software is licensed to you under (1) the MaidSafe.net Commercial License,
    version 1.0 or later, or (2) The General Public License (GPL), version 3, depending on which
    licence you accepted on initial access to the Software (the "Licences").

    By contributing code to the SAFE Network Software, or to this project generally, you agree to be
    bound by the terms of the MaidSafe Contributor Agreement, version 1.0.  This, along with the
    Licenses can be found in the root directory of this project at LICENSE, COPYING and CONTRIBUTOR.

    Unless required by applicable law or agreed to in writing, the SAFE Network Software distributed
    under the GPL Licence is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
    ANY KIND, either express or implied.

    Please review the Licences for the specific language governing permissions and limitations
    relating to use of the SAFE Network Software.                                                 */

#include "choose_total_share_count.hpp"

#include <iostream>
#include <limits>
#include <memory>

#include "choose_threshold.hpp"
#include "tool.hpp"

ChooseTotalShareCount::ChooseTotalShareCount(Tool* tool)
    : Command(tool, "Choose total share count.",
              "\nThis is the number of employees who should each hold a secret share of the\n"
              "company key and password.  It must be at least 3.\n" +
                  kPrompt_) {}

void ChooseTotalShareCount::GetChoice() {
  std::cout << kInstructions_;
  while (!DoGetChoice(tool_->total_share_count, static_cast<int*>(nullptr), 3,
                      std::numeric_limits<int>::max()))
    std::cout << '\n' << kInstructions_;
}

std::unique_ptr<Command> ChooseTotalShareCount::HandleChoice() {
  std::cout << kSeparator_;
  return std::make_unique<ChooseThreshold>(tool_);
}
