/*  Copyright 2015 MaidSafe.net limited.

    This SAFE Network Software is licensed to you under (1) the MaidSafe.net Commercial License,
    version 1.0 or later, or (2) The General Public License (GPL), version 3, depending on which
    licence you accepted on initial access to the Software (the "Licences").

    By contributing code to the SAFE Network Software, or to this project generally, you agree to be
    bound by the terms of the MaidSafe Contributor Agreement, version 1.0.  This, along with the
    Licenses can be found in the root directory of this project at LICENSE, COPYING and CONTRIBUTOR.

    Unless required by applicable law or agreed to in writing, the SAFE Network Software distributed
    under the GPL Licence is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
    ANY KIND, either express or implied.

    Please review the Licences for the specific language governing permissions and limitations
    relating to use of the SAFE Network Software.                                                 */

#include "begin.hpp"

#include <cassert>
#include <iostream>
#include <memory>

#include "choose_total_share_count.hpp"
#include "enter_secret_share.hpp"
#include "tool.hpp"

Begin::Begin(Tool* tool)
    : Command(tool, "", "\nPlease choose from the following ('" + kQuitCommand_ +
                  "' at any time to quit):\n\n"
                  "  1. Secret share a company key and password.\n"
                  "  2. Reconstruct a company key and password.\n" +
                  kPrompt_),
      choice_(0) {}

void Begin::GetChoice() {
  std::cout << kInstructions_;
  while (!DoGetChoice(choice_, static_cast<int*>(nullptr), 1, 2))
    std::cout << '\n' << kInstructions_;
}

std::unique_ptr<Command> Begin::HandleChoice() {
  std::cout << kSeparator_;
  switch (choice_) {
    case 1:
      tool_->reconstruct = false;
      return std::make_unique<ChooseTotalShareCount>(tool_);
    case 2:
      tool_->reconstruct = true;
      return std::make_unique<EnterSecretShare>(tool_);
    default:
      assert(false);
      return nullptr;
  }
}
