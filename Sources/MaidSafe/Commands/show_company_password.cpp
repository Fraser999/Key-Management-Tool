/*  Copyright 2015 MaidSafe.net limited.

    This SAFE Network Software is licensed to you under (1) the MaidSafe.net Commercial License,
    version 1.0 or later, or (2) The General Public License (GPL), version 3, depending on which
    licence you accepted on initial access to the Software (the "Licences").

    By contributing code to the SAFE Network Software, or to this project generally, you agree to be
    bound by the terms of the MaidSafe Contributor Agreement, version 1.0.  This, along with the
    Licenses can be found in the root directory of this project at LICENSE, COPYING and CONTRIBUTOR.

    Unless required by applicable law or agreed to in writing, the SAFE Network Software distributed
    under the GPL Licence is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
    ANY KIND, either express or implied.

    Please review the Licences for the specific language governing permissions and limitations
    relating to use of the SAFE Network Software.                                                 */

#include "show_company_password.hpp"

#include <iostream>

#include "tool.hpp"

ShowCompanyPassword::ShowCompanyPassword(Tool* tool)
    : Command(tool, "Do you wish to show the company password?", " (Y/n)" + kPrompt_) {}

void ShowCompanyPassword::GetChoice() {
  std::cout << kInstructions_;
  bool show(false), default_choice(true);
  while (!DoGetChoice(show, &default_choice))
    std::cout << '\n' << kInstructions_;
}

std::unique_ptr<Command> ShowCompanyPassword::HandleChoice() {
  std::cout << kSeparator_ << '\n' << tool_->company_password << '\n' << kSeparator_
            << "\nRecovery complete.\n\n";
  throw Tool::kQuit_;
}
