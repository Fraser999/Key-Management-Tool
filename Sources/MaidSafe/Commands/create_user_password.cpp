/*  Copyright 2015 MaidSafe.net limited.

    This SAFE Network Software is licensed to you under (1) the MaidSafe.net Commercial License,
    version 1.0 or later, or (2) The General Public License (GPL), version 3, depending on which
    licence you accepted on initial access to the Software (the "Licences").

    By contributing code to the SAFE Network Software, or to this project generally, you agree to be
    bound by the terms of the MaidSafe Contributor Agreement, version 1.0.  This, along with the
    Licenses can be found in the root directory of this project at LICENSE, COPYING and CONTRIBUTOR.

    Unless required by applicable law or agreed to in writing, the SAFE Network Software distributed
    under the GPL Licence is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
    ANY KIND, either express or implied.

    Please review the Licences for the specific language governing permissions and limitations
    relating to use of the SAFE Network Software.                                                 */

#include "create_user_password.hpp"

#include <iostream>
#include <memory>

#include "tool.hpp"
#include "utils.hpp"

CreateUserPassword::CreateUserPassword(Tool* tool, std::size_t user_index, std::string password)
    : Command(tool, AsWord(user_index + 1) + " user: " + (password.empty() ? "enter" : "confirm") +
                  " your password.",
              (password.empty() ? "\nThe password must be at least 8 characters long.\n" : "") +
                  kPrompt_),
      user_index_(user_index),
      password_(password),
      password_confirmed_(false) {}

void CreateUserPassword::GetChoice() {
  std::cout << kInstructions_;
  EchoConsoleInput(false);
  std::string password;
  while (!DoGetChoice(password, static_cast<std::string*>(nullptr)))
    std::cout << '\n' << kInstructions_;
  EchoConsoleInput(true);

  if (password_.empty()) {
    // TODO(Fraser#5#): 2015-10-04 - Improve validation?  or remove it?
    if (password.size() < 8) {
      std::cout << "\nPassword too short.";
      return GetChoice();
    }
    password_ = password;
  } else if (password_ != password) {
    std::cout << "\n!!! Passwords don't match.  Start over!!!\n";
    password_.clear();
  } else {
    password_confirmed_ = true;
  }
}

std::unique_ptr<Command> CreateUserPassword::HandleChoice() {
  if (password_.empty()) {  // User failed to enter same password twice
    std::cout << kSeparator_;
    return std::make_unique<CreateUserPassword>(tool_, user_index_);
  }

  if (password_confirmed_) {
    std::cout << "\nSecret share:\n"
              << EncryptShare(tool_->secret_shares.at(user_index_), password_) << '\n'
              << kSeparator_;
    if (user_index_ + 1 == static_cast<std::size_t>(tool_->total_share_count)) {
      std::cout << "\nSecret sharing complete.\n\n";
      throw Tool::kQuit_;
    }
    return std::make_unique<CreateUserPassword>(tool_, user_index_ + 1);
  }

  return std::make_unique<CreateUserPassword>(tool_, user_index_, password_);
}
