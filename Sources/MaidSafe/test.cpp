/*  Copyright 2015 MaidSafe.net limited.

    This SAFE Network Software is licensed to you under (1) the MaidSafe.net Commercial License,
    version 1.0 or later, or (2) The General Public License (GPL), version 3, depending on which
    licence you accepted on initial access to the Software (the "Licences").

    By contributing code to the SAFE Network Software, or to this project generally, you agree to be
    bound by the terms of the MaidSafe Contributor Agreement, version 1.0.  This, along with the
    Licenses can be found in the root directory of this project at LICENSE, COPYING and CONTRIBUTOR.

    Unless required by applicable law or agreed to in writing, the SAFE Network Software distributed
    under the GPL Licence is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
    ANY KIND, either express or implied.

    Please review the Licences for the specific language governing permissions and limitations
    relating to use of the SAFE Network Software.                                                 */

#define CATCH_CONFIG_MAIN

#include <chrono>
#include <cstdint>
#include <limits>
#include <random>

#include "catch.hpp"

#include "tool.hpp"
#include "utils.hpp"

std::uint32_t RandomNumber(std::uint32_t min = 0,
                           std::uint32_t max = std::numeric_limits<std::uint32_t>::max()) {
  static std::mt19937 random_number_generator(
      static_cast<uint32_t>(std::chrono::high_resolution_clock::now().time_since_epoch().count()));
  std::uniform_int_distribution<std::uint32_t> distribution(min, max);
  return distribution(random_number_generator);
}

std::string RandomString(std::uint32_t min, std::uint32_t max) {
  static std::mt19937 random_number_generator(RandomNumber());
  std::uint32_t size(RandomNumber(min, max));
  std::uniform_int_distribution<> distribution(0, 255);
  std::string random_string(size, 0);
  {
    std::generate(random_string.begin(), random_string.end(),
                  [&] { return distribution(random_number_generator); });
  }
  return random_string;
}

TEST_CASE("Hex Encode and Decode", "[utils]") {
  CHECK(HexEncode("\x1\x23\x45\x67\x89\xab\xcd\xef") == "0123456789abcdef");
  CHECK(HexDecode("0123456789abcdef") == "\x1\x23\x45\x67\x89\xab\xcd\xef");
}

TEST_CASE("SHA512", "[utils]") {
  CHECK(SHA512("abc") == HexDecode(
                             "ddaf35a193617abacc417349ae20413112e6fa4e89a97ea20a9eeee64b55d39a21929"
                             "92a274fc1a836ba3c23a3feebbd454d4423643ce80e2a9ac94fa54ca49f"));
  CHECK(SHA512(std::string(64 * 15625, 'a')) ==
        HexDecode(
            "e718483d0ce769644e2e42c7bc15b4638e1f98b13b2044285632a803afa973ebde0ff244877ea60a4cb043"
            "2ce577c31beb009c5c2c49aa2e4eadb217ad8cc09b"));
  CHECK(SHA512(
            "abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnop"
            "qrlmnopqrsmnopqrstnopqrstu") ==
        HexDecode(
            "8e959b75dae313da8cf4f72814fc143f8f7779c6eb9f7fa17299aeadb6889018501d289e4900f7e4331b99"
            "dec4b5433ac7d329eeb6dd26545e96e55b874be909"));
}

TEST_CASE("AsWord", "[utils]") {
  CHECK(AsWord(1) == "1st");
  CHECK(AsWord(2) == "2nd");
  CHECK(AsWord(3) == "3rd");
  CHECK(AsWord(4) == "4th");
  CHECK(AsWord(5) == "5th");
  CHECK(AsWord(6) == "6th");
  CHECK(AsWord(7) == "7th");
  CHECK(AsWord(8) == "8th");
  CHECK(AsWord(9) == "9th");
  CHECK(AsWord(10) == "10th");
  CHECK(AsWord(11) == "11th");
  CHECK(AsWord(12) == "12th");
  CHECK(AsWord(13) == "13th");
  CHECK(AsWord(14) == "14th");
  CHECK(AsWord(15) == "15th");
  CHECK(AsWord(16) == "16th");
  CHECK(AsWord(17) == "17th");
  CHECK(AsWord(18) == "18th");
  CHECK(AsWord(19) == "19th");
  CHECK(AsWord(20) == "20th");
  CHECK(AsWord(21) == "21st");
  CHECK(AsWord(22) == "22nd");
  CHECK(AsWord(23) == "23rd");
  CHECK(AsWord(24) == "24th");
  CHECK(AsWord(25) == "25th");
  CHECK(AsWord(26) == "26th");
  CHECK(AsWord(27) == "27th");
  CHECK(AsWord(28) == "28th");
  CHECK(AsWord(29) == "29th");
  CHECK(AsWord(30) == "30th");
  CHECK(AsWord(99) == "99th");
  CHECK(AsWord(100) == "100th");
  CHECK(AsWord(101) == "101st");
  CHECK(AsWord(102) == "102nd");
  CHECK(AsWord(103) == "103rd");
  CHECK(AsWord(104) == "104th");
  CHECK(AsWord(105) == "105th");
  CHECK(AsWord(106) == "106th");
  CHECK(AsWord(107) == "107th");
  CHECK(AsWord(108) == "108th");
  CHECK(AsWord(109) == "109th");
  CHECK(AsWord(110) == "110th");
  CHECK(AsWord(111) == "111th");
  CHECK(AsWord(112) == "112th");
  CHECK(AsWord(113) == "113th");
  CHECK(AsWord(114) == "114th");
  CHECK(AsWord(115) == "115th");
  CHECK(AsWord(116) == "116th");
  CHECK(AsWord(117) == "117th");
  CHECK(AsWord(118) == "118th");
  CHECK(AsWord(119) == "119th");
  CHECK(AsWord(120) == "120th");
  CHECK(AsWord(121) == "121st");
  CHECK(AsWord(122) == "122nd");
  CHECK(AsWord(123) == "123rd");
  CHECK(AsWord(124) == "124th");
  CHECK(AsWord(199) == "199th");
  CHECK(AsWord(200) == "200th");
  CHECK(AsWord(201) == "201st");
  CHECK(AsWord(202) == "202nd");
  CHECK(AsWord(203) == "203rd");
  CHECK(AsWord(204) == "204th");
  CHECK(AsWord(205) == "205th");
  CHECK(AsWord(206) == "206th");
  CHECK(AsWord(207) == "207th");
  CHECK(AsWord(208) == "208th");
  CHECK(AsWord(209) == "209th");
  CHECK(AsWord(210) == "210th");
  CHECK(AsWord(211) == "211th");
  CHECK(AsWord(212) == "212th");
  CHECK(AsWord(213) == "213th");
  CHECK(AsWord(214) == "214th");
  CHECK(AsWord(215) == "215th");
  CHECK(AsWord(216) == "216th");
  CHECK(AsWord(217) == "217th");
  CHECK(AsWord(218) == "218th");
  CHECK(AsWord(219) == "219th");
  CHECK(AsWord(220) == "220th");
  CHECK(AsWord(221) == "221st");
  CHECK(AsWord(222) == "222nd");
  CHECK(AsWord(223) == "223rd");
  CHECK(AsWord(224) == "224th");
  CHECK(AsWord(999) == "999th");
  CHECK(AsWord(1000) == "1000th");
  CHECK(AsWord(1001) == "1001st");
  CHECK(AsWord(1002) == "1002nd");
  CHECK(AsWord(1003) == "1003rd");
  CHECK(AsWord(1004) == "1004th");
  CHECK(AsWord(1005) == "1005th");
  CHECK(AsWord(1006) == "1006th");
  CHECK(AsWord(1007) == "1007th");
  CHECK(AsWord(1008) == "1008th");
  CHECK(AsWord(1009) == "1009th");
  CHECK(AsWord(1010) == "1010th");
  CHECK(AsWord(1011) == "1011th");
  CHECK(AsWord(1012) == "1012th");
  CHECK(AsWord(1013) == "1013th");
  CHECK(AsWord(1014) == "1014th");
  CHECK(AsWord(1015) == "1015th");
  CHECK(AsWord(1016) == "1016th");
  CHECK(AsWord(1017) == "1017th");
  CHECK(AsWord(1018) == "1018th");
  CHECK(AsWord(1019) == "1019th");
  CHECK(AsWord(1020) == "1020th");
  CHECK(AsWord(1021) == "1021st");
  CHECK(AsWord(1022) == "1022nd");
  CHECK(AsWord(1023) == "1023rd");
  CHECK(AsWord(1024) == "1024th");
  CHECK(AsWord(1099) == "1099th");
  CHECK(AsWord(1100) == "1100th");
  CHECK(AsWord(1101) == "1101st");
  CHECK(AsWord(1102) == "1102nd");
  CHECK(AsWord(1103) == "1103rd");
  CHECK(AsWord(1104) == "1104th");
  CHECK(AsWord(1105) == "1105th");
  CHECK(AsWord(1106) == "1106th");
  CHECK(AsWord(1107) == "1107th");
  CHECK(AsWord(1108) == "1108th");
  CHECK(AsWord(1109) == "1109th");
  CHECK(AsWord(1110) == "1110th");
  CHECK(AsWord(1111) == "1111th");
  CHECK(AsWord(1112) == "1112th");
  CHECK(AsWord(1113) == "1113th");
  CHECK(AsWord(1114) == "1114th");
  CHECK(AsWord(1115) == "1115th");
  CHECK(AsWord(1116) == "1116th");
  CHECK(AsWord(1117) == "1117th");
  CHECK(AsWord(1118) == "1118th");
  CHECK(AsWord(1119) == "1119th");
  CHECK(AsWord(1120) == "1120th");
  CHECK(AsWord(1121) == "1121st");
  CHECK(AsWord(1122) == "1122nd");
  CHECK(AsWord(1123) == "1123rd");
  CHECK(AsWord(1124) == "1124th");
  CHECK(AsWord(1199) == "1199th");
  CHECK(AsWord(1200) == "1200th");
  CHECK(AsWord(1201) == "1201st");
  CHECK(AsWord(1202) == "1202nd");
  CHECK(AsWord(1203) == "1203rd");
  CHECK(AsWord(1204) == "1204th");
  CHECK(AsWord(1205) == "1205th");
  CHECK(AsWord(1206) == "1206th");
  CHECK(AsWord(1207) == "1207th");
  CHECK(AsWord(1208) == "1208th");
  CHECK(AsWord(1209) == "1209th");
  CHECK(AsWord(1210) == "1210th");
  CHECK(AsWord(1211) == "1211th");
  CHECK(AsWord(1212) == "1212th");
  CHECK(AsWord(1213) == "1213th");
  CHECK(AsWord(1214) == "1214th");
  CHECK(AsWord(1215) == "1215th");
  CHECK(AsWord(1216) == "1216th");
  CHECK(AsWord(1217) == "1217th");
  CHECK(AsWord(1218) == "1218th");
  CHECK(AsWord(1219) == "1219th");
  CHECK(AsWord(1220) == "1220th");
  CHECK(AsWord(1221) == "1221st");
  CHECK(AsWord(1222) == "1222nd");
  CHECK(AsWord(1223) == "1223rd");
  CHECK(AsWord(1224) == "1224th");
}

TEST_CASE("Secret Share and Recover", "[utils]") {
  Tool tool;
  for (int i(0); i < 20; ++i) {
    const int kTotalShareCount(RandomNumber(4, 50));
    const std::string kCompanyPassword(RandomString(2, 1000));
    tool.total_share_count = kTotalShareCount;
    tool.threshold = kTotalShareCount * 8 / 10;
    tool.company_password = kCompanyPassword;

    // Check correct number of secret shares are generated.
    tool.secret_shares.clear();
    GenerateShares(&tool);
    CHECK(tool.secret_shares.size() == kTotalShareCount);

    // Check that the password can be recovered with the order of the secret shares shuffled.
    tool.company_password.clear();
    std::mt19937 random_number_generator(RandomNumber());
    std::shuffle(tool.secret_shares.begin(), tool.secret_shares.end(), random_number_generator);
    RecoverData(&tool);
    CHECK(tool.company_password == kCompanyPassword);

    // Check that the password can be recovered with only `threshold` secret shares available.
    tool.company_password.clear();
    tool.secret_shares.resize(tool.threshold);
    RecoverData(&tool);
    CHECK(tool.company_password == kCompanyPassword);

    // Check that the password cannot be recovered with less than `threshold` secret shares available.
    tool.company_password.clear();
    tool.secret_shares.resize(tool.threshold - 1);
    RecoverData(&tool);
    CHECK(tool.company_password.empty());
  }
}

TEST_CASE("Encrypt and Decrypt Share", "[utils]") {
  for (int i(0); i < 100; ++i) {
    const std::string kShare(RandomString(10, 10000));
    const std::string kPassword(RandomString(2, 100));

    const std::string kEncryptedShare(EncryptShare(kShare, kPassword));
    CHECK(!kEncryptedShare.empty());

    CHECK(DecryptShare(kEncryptedShare, kPassword) == kShare);
    CHECK(DecryptShare(kEncryptedShare, kPassword.substr(1)).empty());
    CHECK(DecryptShare(kEncryptedShare.substr(1), kPassword).empty());
  }
}
