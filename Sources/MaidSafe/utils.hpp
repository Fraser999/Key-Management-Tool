/*  Copyright 2015 MaidSafe.net limited.

    This SAFE Network Software is licensed to you under (1) the MaidSafe.net Commercial License,
    version 1.0 or later, or (2) The General Public License (GPL), version 3, depending on which
    licence you accepted on initial access to the Software (the "Licences").

    By contributing code to the SAFE Network Software, or to this project generally, you agree to be
    bound by the terms of the MaidSafe Contributor Agreement, version 1.0.  This, along with the
    Licenses can be found in the root directory of this project at LICENSE, COPYING and CONTRIBUTOR.

    Unless required by applicable law or agreed to in writing, the SAFE Network Software distributed
    under the GPL Licence is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
    ANY KIND, either express or implied.

    Please review the Licences for the specific language governing permissions and limitations
    relating to use of the SAFE Network Software.                                                 */

#ifndef UTILS_HPP_
#define UTILS_HPP_

#include <string>

struct Tool;

void EchoConsoleInput(bool enable);

std::string HexEncode(const std::string& non_hex_input);

std::string HexDecode(const std::string& hex_input);

std::string SHA512(const std::string& input);

void GenerateShares(Tool* const tool);

void RecoverData(Tool* const tool);

std::string EncryptShare(const std::string& share, const std::string& password);

std::string DecryptShare(std::string share, const std::string& password);

// Turns "1" to "1st", "2" to "2nd", etc.  index must be > 0.
std::string AsWord(std::size_t index);

#endif  // UTILS_HPP_
