/*  Copyright 2015 MaidSafe.net limited.

    This SAFE Network Software is licensed to you under (1) the MaidSafe.net Commercial License,
    version 1.0 or later, or (2) The General Public License (GPL), version 3, depending on which
    licence you accepted on initial access to the Software (the "Licences").

    By contributing code to the SAFE Network Software, or to this project generally, you agree to be
    bound by the terms of the MaidSafe Contributor Agreement, version 1.0.  This, along with the
    Licenses can be found in the root directory of this project at LICENSE, COPYING and CONTRIBUTOR.

    Unless required by applicable law or agreed to in writing, the SAFE Network Software distributed
    under the GPL Licence is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
    ANY KIND, either express or implied.

    Please review the Licences for the specific language governing permissions and limitations
    relating to use of the SAFE Network Software.                                                 */

#include "utils.hpp"

#include <cassert>
#include <iostream>
#include <vector>

#ifdef _MSC_VER
#include <windows.h>
#pragma warning(push)
#pragma warning(disable : 4100 4127 4189 4244)
#else
#include <unistd.h>
#include <termios.h>
#endif
#include "aes.h"
#include "channels.h"
#include "gcm.h"
#include "osrng.h"
#include "filters.h"
#include "ida.h"
#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include "tool.hpp"

namespace {

const int kSymmetricKeySize = 32;
const int kSymmetricIvSize = 16;
const unsigned char kHexAlphabet[] = "0123456789abcdef";
const unsigned char kHexLookup[] = {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0,  0,  0,  0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0,  0,  1,  2, 3,
    4, 5, 6, 7, 8, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0,  0,  0,  0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 11, 12, 13, 14, 15};
const std::string kValidator = "Secret Share Appendix";

CryptoPP::RandomNumberGenerator& RandomNumberGenerator() {
  static CryptoPP::AutoSeededX917RNG<CryptoPP::AES> random_number_generator;
  return random_number_generator;
}

}  // unnamed namespace

void EchoConsoleInput(bool enable) {
#ifdef _MSC_VER
  HANDLE handle_to_stdin = GetStdHandle(STD_INPUT_HANDLE);
  DWORD mode;
  GetConsoleMode(handle_to_stdin, &mode);
  enable ? mode |= ENABLE_ECHO_INPUT : mode &= ~ENABLE_ECHO_INPUT;
  SetConsoleMode(handle_to_stdin, mode);
#else
  struct termios tty;
  tcgetattr(STDIN_FILENO, &tty);
  enable ? tty.c_lflag |= ECHO : tty.c_lflag &= ~ECHO;
  tcsetattr(STDIN_FILENO, TCSANOW, &tty);
#endif
}

std::string SHA512(const std::string& input) {
  std::string output;
  CryptoPP::SHA512 hash;
  CryptoPP::StringSource(input, true,
                         new CryptoPP::HashFilter(hash, new CryptoPP::StringSink(output)));
  return output;
}

std::string HexEncode(const std::string& non_hex_input) {
  auto size(non_hex_input.size());
  std::string hex_output(size * 2, 0);
  for (std::size_t i(0), j(0); i != size; ++i) {
    hex_output[j++] = kHexAlphabet[static_cast<unsigned char>(non_hex_input[i]) / 16];
    hex_output[j++] = kHexAlphabet[static_cast<unsigned char>(non_hex_input[i]) % 16];
  }
  return hex_output;
}

std::string HexDecode(const std::string& hex_input) {
  auto size(hex_input.size());
  if (size % 2)  // Sanity check
    throw 5;

  std::string non_hex_output(size / 2, 0);
  for (size_t i(0), j(0); i != size / 2; ++i) {
    non_hex_output[i] = (kHexLookup[static_cast<int>(hex_input[j++])] << 4);
    non_hex_output[i] |= kHexLookup[static_cast<int>(hex_input[j++])];
  }
  return non_hex_output;
}

void GenerateShares(Tool* const tool) {
  assert(tool->total_share_count >= 3);
  assert(tool->threshold >= 2 && tool->threshold <= tool->total_share_count);

  // Append known data to allow validation of recovery
  std::string input(tool->company_password + kValidator);

  auto channel_switch = new CryptoPP::ChannelSwitch;
  CryptoPP::StringSource source(
      input, false, new CryptoPP::SecretSharing(RandomNumberGenerator(), tool->threshold,
                                                tool->total_share_count, channel_switch));

  CryptoPP::vector_member_ptrs<CryptoPP::StringSink> string_sink(tool->total_share_count);
  tool->secret_shares.resize(tool->total_share_count);
  std::string channel;

  for (int i = 0; i < tool->total_share_count; ++i) {
    string_sink[i].reset(new CryptoPP::StringSink(tool->secret_shares[i]));
    channel = CryptoPP::WordToString<CryptoPP::word32>(i);
    string_sink[i]->Put(reinterpret_cast<const byte*>(channel.data()), 4);
    channel_switch->AddRoute(channel, *string_sink[i], CryptoPP::DEFAULT_CHANNEL);
  }
  source.PumpAll();
}

void RecoverData(Tool* const tool) {
  int parts_count = static_cast<int>(tool->secret_shares.size());
  if (parts_count < 2)
    return;

  std::string recovered;
  CryptoPP::SecretRecovery recovery(parts_count, new CryptoPP::StringSink(recovered));
  CryptoPP::vector_member_ptrs<CryptoPP::StringSource> string_sources(parts_count);
  CryptoPP::SecByteBlock channel(4);

  for (std::size_t i = 0; i < static_cast<std::size_t>(parts_count); ++i) {
    string_sources[i].reset(new CryptoPP::StringSource(tool->secret_shares[i], false));
    string_sources[i]->Pump(4);
    string_sources[i]->Get(channel, 4);
    string_sources[i]->Attach(new CryptoPP::ChannelSwitch(
        recovery, std::string(reinterpret_cast<char*>(channel.begin()), 4)));
  }

  for (std::size_t i = 0; i < static_cast<std::size_t>(parts_count); ++i)
    string_sources[i]->PumpAll();

  if (recovered.size() <= kValidator.size() ||
      recovered.substr(recovered.size() - kValidator.size()) != kValidator)
    return;

  tool->company_password = recovered.substr(0, recovered.size() - kValidator.size());
}

std::string EncryptShare(const std::string& share, const std::string& password) {
  // Get SHA512 of user's password to give enough data for an AES key (32 bytes) and IV (16 bytes).
  std::string hash(SHA512(password));
  CryptoPP::GCM<CryptoPP::AES, CryptoPP::GCM_64K_Tables>::Encryption encryptor;
  encryptor.SetKeyWithIV(reinterpret_cast<const byte*>(hash.data()), kSymmetricKeySize,
                         reinterpret_cast<const byte*>(hash.data()) + kSymmetricKeySize,
                         kSymmetricIvSize);

  // AES encrypt the share and display the result in hex-encoded format.
  std::string result;
  CryptoPP::StringSource(share, true, new CryptoPP::AuthenticatedEncryptionFilter(
                                          encryptor, new CryptoPP::StringSink(result)));
  return HexEncode(result);
}

std::string DecryptShare(std::string share, const std::string& password) {
  // Decode from hex
  try {
    share = HexDecode(share);
  } catch (int) {
    share.clear();
  }

  // Get SHA512 of user's password to yield AES key (32 bytes) and IV (16 bytes).
  std::string hash(SHA512(password));

  // Decrypt
  std::string result;
  try {
    CryptoPP::GCM<CryptoPP::AES, CryptoPP::GCM_64K_Tables>::Decryption decryptor;
    decryptor.SetKeyWithIV(reinterpret_cast<const byte*>(hash.data()), kSymmetricKeySize,
                           reinterpret_cast<const byte*>(hash.data()) + kSymmetricKeySize,
                           kSymmetricIvSize);
    CryptoPP::StringSource(share, true, new CryptoPP::AuthenticatedDecryptionFilter(
                                            decryptor, new CryptoPP::StringSink(result)));
  } catch (const CryptoPP::Exception&) {
    result.clear();
  }
  return result;
}

std::string AsWord(std::size_t index) {
  assert(index > 0);
  if (index % 10 == 1 && index % 100 != 11) {
    return std::to_string(index) + "st";
  }
  if (index % 10 == 2 && index % 100 != 12) {
    return std::to_string(index) + "nd";
  }
  if (index % 10 == 3 && index % 100 != 13) {
    return std::to_string(index) + "rd";
  }
  return std::to_string(index) + "th";
}
