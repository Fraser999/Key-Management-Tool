# MaidSafe Key Management Tool

Tool to allow several users to securely share a company password.

[![build status](https://gitlab.com/ci/projects/9880/status.png?ref=master)](https://gitlab.com/ci/projects/9880?ref=master)
