#==================================================================================================#
#  Copyright 2015 MaidSafe.net limited.                                                            #
#                                                                                                  #
#  This MaidSafe Software is licensed to you under (1) the MaidSafe.net Commercial License,        #
#  version 1.0 or later, or (2) The General Public License (GPL), version 3, depending on which    #
#  licence you accepted on initial access to the Software (the "Licences").                        #
#                                                                                                  #
#  By contributing code to the MaidSafe Software, or to this project generally, you agree to be    #
#  bound by the terms of the MaidSafe Contributor Agreement, version 1.0.  This, along with the    #
#  Licenses can be found in the root directory of this project at LICENSE, COPYING and             #
#  CONTRIBUTOR.                                                                                    #
#                                                                                                  #
#  Unless required by applicable law or agreed to in writing, the MaidSafe Software distributed    #
#  under the GPL Licence is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF   #
#  ANY KIND, either express or implied.                                                            #
#                                                                                                  #
#  Please review the Licences for the specific language governing permissions and limitations      #
#  relating to the use of the MaidSafe Software.                                                   #
#==================================================================================================#
#                                                                                                  #
#  Utility functions.                                                                              #
#                                                                                                  #
#==================================================================================================#

function(check_compiler)
  # If the path to the CMAKE_CXX_COMPILER doesn't change, CMake doesn't detect a version change
  # in the compiler.  We cache the output of running the compiler with '--version' and check
  # on each subsequent configure that the output is identical.  Note, with MSVC the command
  # fails ('--version' is an unrecognised arg), but still outputs the compiler version.
  execute_process(COMMAND ${CMAKE_CXX_COMPILER} --version
                  OUTPUT_VARIABLE OutputVar ERROR_VARIABLE ErrorVar)
  string(REPLACE "\n" ";" CombinedOutput "${OutputVar}${ErrorVar}")
  if(CheckCompilerVersion)
    if(NOT CheckCompilerVersion STREQUAL CombinedOutput)
      set(Msg "\n\nThe C++ compiler \"${CMAKE_CXX_COMPILER}\" has changed since the previous run of CMake.")
      set(Msg "${Msg}  This requires a clean build folder, so either delete all contents from this")
      set(Msg "${Msg}  folder, or create a new one and run CMake from there.\n\n")
      message(FATAL_ERROR "${Msg}")
    endif()
  else()
    set(CheckCompilerVersion "${CombinedOutput}" CACHE INTERNAL "")
  endif()
  if("x${CMAKE_CXX_COMPILER_ID}" STREQUAL "xMSVC")
    if(CMAKE_CXX_COMPILER_VERSION VERSION_LESS 19)  # i.e for MSVC < Visual Studio 2015
      message(FATAL_ERROR "\nIn order to use C++11 features, this library cannot be built using a version of Visual Studio less than 2015.")
    endif()
  elseif(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    if(CMAKE_CXX_COMPILER_VERSION VERSION_LESS "3.3")
      message(FATAL_ERROR "\nIn order to use C++11 features, this library cannot be built using a version of Clang less than 3.3")
    endif()
  elseif(CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang")
    if(CMAKE_CXX_COMPILER_VERSION VERSION_LESS "5.1")
      message(FATAL_ERROR "\nIn order to use C++11 features, this library cannot be built using a version of AppleClang less than 5.1")
    endif()
  elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    if(CMAKE_CXX_COMPILER_VERSION VERSION_LESS "4.9")
      message(FATAL_ERROR "\nIn order to use C++11 features, this library cannot be built using a version of GCC less than 4.9")
    endif()
  endif()
endfunction()

# Workaround for the Xcode's missing ability to pass -isystem to the compiler.
function(target_include_system_dirs Target)
  if(XCODE OR UNIX)
    foreach(Arg ${ARGN})
      string(REGEX MATCH "\\$<" IsGeneratorExpression "${Arg}")
      if(Arg STREQUAL "PRIVATE" OR Arg STREQUAL "PUBLIC" OR Arg STREQUAL "INTERFACE")
        set(Scope ${Arg})
      elseif(NOT IsGeneratorExpression STREQUAL "")
        message(AUTHOR_WARNING "This function doesn't handle generator expressions; skipping ${Arg}")
      else()
        target_compile_options(${Target} ${Scope} -isystem${Arg})
      endif()
    endforeach()
  else()
    target_include_directories(${Target} SYSTEM ${Scope} ${ARGN})
  endif()
endfunction()
